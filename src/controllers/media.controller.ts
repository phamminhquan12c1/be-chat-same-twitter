import { NextFunction, Request, Response } from 'express'
import { dataResponse } from '~/utils/response'
import mediaService from '~/services/media.service'
export const uploadSingleImageController = async (req: Request, res: Response, next: NextFunction) => {
    const result = await mediaService.handleUploadSingleImage(req)
    return res.status(200).json(dataResponse(200, result, 'Upload image successfully'))
}
