import { NextFunction, Request, Response } from 'express'
import {
    ChangPasswordRequestBody,
    FollowRequestBody,
    ForgotRequestPayload,
    LogoutRequestBody,
    ParamsGetProfileRequestPayload,
    ParamsUnfollowRequestPayload,
    TokenPayload,
    UpdateMeRequestBody,
    UserRegisterRequest,
    VerifyForgotPasswordTokenRequestPayload,
    resetPasswordPayload
} from '~/models/requests/users.requests'
import userService from '~/services/user.service'
import { ParamsDictionary } from 'express-serve-static-core'
import { dataResponse } from '~/utils/response'
import User from '~/models/schemas/User.schema'
import { ObjectId } from 'mongodb'
import databaseService from '~/services/database.service'
import { UserVerifyStatus } from '~/constants/enums/user.enum'

export const registerController = async (
    req: Request<ParamsDictionary, any, UserRegisterRequest>,
    res: Response,
    next: NextFunction
) => {
    const result = await userService.register(req.body)
    return res.status(200).json(dataResponse(200, result, 'User created successfully'))
}

export const loginController = async (req: Request, res: Response, next: NextFunction) => {
    const user = req.user as User
    const userId = user._id as ObjectId
    const result = await userService.login({ userId: userId.toString(), verify: user.verify })
    return res.status(200).json(dataResponse(200, result, 'Login successfully'))
}

export const logoutController = async (
    req: Request<ParamsDictionary, any, LogoutRequestBody>,
    res: Response,
    next: NextFunction
) => {
    const { refresh_token } = req.body
    const result = await userService.logout(refresh_token)
    return res.status(200).json(dataResponse(200, result, result.message || 'Logout successfully'))
}

export const emailVerifyController = async (req: Request, res: Response, next: NextFunction) => {
    const { userId } = req.decode_email_verify_token as TokenPayload
    const user = await databaseService.users.findOne({ _id: new ObjectId(userId) })

    if (!user) {
        return res.status(404).json(dataResponse(404, null, 'User not found'))
    }

    if (user.email_verify_token === '') {
        return res.status(200).json(dataResponse(200, null, 'Email already verified before'))
    }

    const result = await userService.verifyEmail(userId)
    return res.status(200).json(dataResponse(200, result, 'Email verified successfully'))
}

export const resendEmailVerifyController = async (req: Request, res: Response, next: NextFunction) => {
    const { userId } = req.decode_authorization as TokenPayload
    const user = await databaseService.users.findOne({ _id: new ObjectId(userId) })
    if (!user) {
        return res.status(404).json(dataResponse(404, null, 'User not found'))
    }

    if (user.verify === UserVerifyStatus.Verified) {
        return res.status(200).json(dataResponse(200, null, 'Email already verified'))
    }

    const result = await userService.resendVerifyEmail(userId)

    return res.status(200).json(dataResponse(200, result?.message, 'Email resend successfully'))
}

export const forgotPasswordController = async (
    req: Request<ParamsDictionary, any, ForgotRequestPayload>,
    res: Response,
    next: NextFunction
) => {
    const { _id, verify } = req.user as User
    const result = await userService.forgotPassword({ userId: (_id as ObjectId).toString(), verify: verify })
    return res.status(200).json(dataResponse(200, result, result?.message || 'Forgot password successfully'))
}

export const verifyForgotPassswordTokenController = async (
    req: Request<ParamsDictionary, any, VerifyForgotPasswordTokenRequestPayload>,
    res: Response,
    next: NextFunction
) => {
    return res.status(200).json(dataResponse(200, [], 'Verify forgot password token successfully'))
}

export const resetPasswordController = async (
    req: Request<ParamsDictionary, any, resetPasswordPayload>,
    res: Response,
    next: NextFunction
) => {
    const { userId } = req.decode_forgot_password_token as TokenPayload
    const { password } = req.body
    const result = await userService.resetPassword(userId, password)
    return res.status(200).json(dataResponse(200, [], result.message || 'Reset password successfully'))
}

export const getMeController = async (req: Request, res: Response, next: NextFunction) => {
    const { userId } = req.decode_authorization as TokenPayload
    const user = await userService.getMe(userId)
    return res.status(200).json(dataResponse(200, user, 'Get my profile successfully'))
}

export const updateMeController = async (
    req: Request<ParamsDictionary, any, UpdateMeRequestBody>,
    res: Response,
    next: NextFunction
) => {
    const { userId } = req.decode_authorization as TokenPayload
    const { body } = req
    const user = await userService.updateMe(userId, body as UpdateMeRequestBody)
    return res.status(200).json(dataResponse(200, user, 'Update my profile successfully'))
}

export const getProfileController = async (
    req: Request<ParamsGetProfileRequestPayload>,
    res: Response,
    next: NextFunction
) => {
    const { username } = req.params
    const user = await userService.profile(username)
    return res.status(200).json(dataResponse(200, user, 'Get profile successfully'))
}

export const followController = async (
    req: Request<ParamsDictionary, any, FollowRequestBody>,
    res: Response,
    next: NextFunction
) => {
    const { userId } = req.decode_authorization as TokenPayload
    const { followed_user_id } = req.body
    const follow = await userService.follow({ userId, followed_user_id })
    return res.status(200).json(dataResponse(200, {}, follow.message || 'Get profile successfully'))
}

export const unFollowController = async (
    req: Request<ParamsUnfollowRequestPayload>,
    res: Response,
    next: NextFunction
) => {
    const { userId } = req.decode_authorization as TokenPayload
    const { followed_user_id } = req.body
    const unFollow = await userService.unFollow({ userId, followed_user_id })
    return res.status(200).json(dataResponse(200, {}, unFollow.message || 'Get profile successfully'))
}

export const changePasswordController = async (
    req: Request<ParamsDictionary, any, ChangPasswordRequestBody>,
    res: Response,
    next: NextFunction
) => {
    const { userId } = req.decode_authorization as TokenPayload
    const { old_password, new_password } = req.body
    const changePassword = await userService.changePassword({ userId, old_password, new_password })
    return res.status(200).json(dataResponse(200, {}, changePassword.message || 'Change password successfully'))
}

export const loginGoogleController = async (
    req: Request<ParamsDictionary, any, ChangPasswordRequestBody>,
    res: Response,
    next: NextFunction
) => {
    const { code } = req.query
    const result = await userService.oauth(code as string)
    const url_redirect =
        `${process.env.CLIENT_SERVER_URL}/login?access_token=${result.access_token}&refresh_token=${result.refresh_token}&new_user=${result.new_user}` as string
    return res.redirect(url_redirect)
}
