import { ObjectId } from 'mongodb'
import { UpdateMeRequestBody, UserRegisterRequest } from '~/models/requests/users.requests'
import databaseService from './database.service'
import User from '~/models/schemas/User.schema'
import { hashPassword } from '~/utils/crypto'
import { signToken } from '~/utils/jwt'
import { TokenType, UserVerifyStatus } from '~/constants/enums/user.enum'
import RefreshToken from '~/models/schemas/RefreshToken.schema'
import { ErrorWithStatus } from '~/models/Errors'
import Followers from '~/models/schemas/Followers.schema'
import axios from 'axios'
import { config } from 'dotenv'
config()
class UserService {
    private signRefreshToken = ({ userId, verify }: { userId: string; verify: UserVerifyStatus }) => {
        return signToken({
            payload: {
                userId,
                typeToken: TokenType.RefreshToken,
                verify: verify
            },
            privateKey: process.env.JWT_SECRET_REFRESH_TOKEN as string,
            options: {
                algorithm: 'HS256',
                expiresIn: '100d'
            }
        })
    }

    private signAccessToken = ({ userId, verify }: { userId: string; verify: UserVerifyStatus }) => {
        return signToken({
            payload: {
                userId,
                typeToken: TokenType.AccessToken,
                verify: verify
            },
            privateKey: process.env.JWT_SECRET_ACCESS_TOKEN as string,
            options: {
                algorithm: 'HS256',
                expiresIn: '15m'
            }
        })
    }

    private signEmailVerifyToken = ({ userId, verify }: { userId: string; verify: UserVerifyStatus }) => {
        return signToken({
            payload: {
                userId,
                typeToken: TokenType.EmailVerifyToken,
                verify: verify
            },
            privateKey: process.env.JWT_SECRET_EMAIL_VERIFY_TOKEN as string,
            options: {
                algorithm: 'HS256',
                expiresIn: '7d'
            }
        })
    }
    private signForgotPasswordToken = ({ userId, verify }: { userId: string; verify: UserVerifyStatus }) => {
        return signToken({
            payload: {
                userId,
                typeToken: TokenType.ForgotPasswordToken,
                verify: verify
            },
            privateKey: process.env.JWT_FORGOT_PASSWORD_TOKEN as string,
            options: {
                algorithm: 'HS256',
                expiresIn: '30m'
            }
        })
    }

    private signAccessAndRefreshToken = ({ userId, verify }: { userId: string; verify: UserVerifyStatus }) => {
        return Promise.all([this.signAccessToken({ userId, verify }), this.signRefreshToken({ userId, verify })])
    }

    async checkExistEmail(email: string) {
        const result = await databaseService.users.findOne({ email })
        if (!result) {
            return false
        }
        return result
    }

    async register(payload: UserRegisterRequest) {
        const userId = new ObjectId()
        const emailVerifyToken = await this.signEmailVerifyToken({
            userId: userId.toString(),
            verify: UserVerifyStatus.Unverified
        })
        await databaseService.users.insertOne(
            new User({
                ...payload,
                _id: userId,
                username: `user${userId.toString()}`,
                email_verify_token: emailVerifyToken,
                date_of_birth: new Date(payload.date_of_birth),
                password: hashPassword(payload.password)
            })
        )

        const [accessToken, refreshToken] = await this.signAccessAndRefreshToken({
            userId: userId.toString(),
            verify: UserVerifyStatus.Unverified
        })

        await databaseService.refreshToken.insertOne(
            new RefreshToken({
                user_id: new ObjectId(userId),
                token: refreshToken
            })
        )
        console.log('Email verify token: ', emailVerifyToken)
        return [accessToken, refreshToken]
    }

    async login({ userId, verify }: { userId: string; verify: UserVerifyStatus }) {
        const [accessToken, refreshToken] = await this.signAccessAndRefreshToken({
            userId: userId,
            verify: verify
        })
        await databaseService.refreshToken.insertOne(
            new RefreshToken({
                user_id: new ObjectId(userId),
                token: refreshToken
            })
        )
        return {
            accessToken,
            refreshToken
        }
    }

    private async getOauthGoogleToken(code: string) {
        const body = {
            code: code,
            client_id: process.env.GOOGLE_CLIENT_ID,
            client_secret: process.env.GOOGLE_CLIENT_SECRET,
            redirect_uri: process.env.GOOGLE_REDIRECT_URI,
            grant_type: 'authorization_code'
        }
        const { data } = await axios.post('https://oauth2.googleapis.com/token', body, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        return data as {
            access_token: string
            id_token: string
        }
    }

    private async getGoogleUserInfo(access_token: string, id_token: string) {
        const { data } = await axios.get('https://www.googleapis.com/oauth2/v1/userinfo', {
            params: {
                access_token,
                alt: 'json'
            },
            headers: {
                Authorization: `Bearer ${id_token}`
            }
        })

        return data as {
            id: string
            email: string
            verified_email: boolean
            name: string
            given_name: string
            family_name: string
            locale: string
        }
    }

    async oauth(code: string) {
        const { id_token, access_token } = await this.getOauthGoogleToken(code)
        const user_info = await this.getGoogleUserInfo(access_token, id_token)
        if (!user_info.verified_email) {
            throw new ErrorWithStatus({
                message: 'User not verified',
                status: 400
            })
        }

        const user_database = await databaseService.users.findOne({ email: user_info.email })

        if (user_database) {
            const [access_token, refresh_token] = await this.signAccessAndRefreshToken({
                userId: user_database._id.toString(),
                verify: user_database.verify
            })
            await databaseService.refreshToken.insertOne(
                new RefreshToken({
                    user_id: user_database._id,
                    token: refresh_token
                })
            )

            return {
                access_token,
                refresh_token,
                new_user: false
            }
        } else {
            const [access_token, refresh_token] = await this.register({
                date_of_birth: new Date().toISOString(),
                email: user_info.email,
                name: user_info.name,
                password: hashPassword(new Date().getMilliseconds.toString())
            })
            return {
                access_token,
                refresh_token,
                new_user: true
            }
        }
    }

    async logout(refreshToken: string) {
        await databaseService.refreshToken.deleteOne({ token: refreshToken })
        return {
            message: 'Logout successfully'
        }
    }

    async verifyEmail(userId: string) {
        const [token] = await Promise.all([
            this.signAccessAndRefreshToken({
                userId,
                verify: UserVerifyStatus.Verified
            }),
            databaseService.users.updateOne(
                { _id: new ObjectId(userId) },
                {
                    $set: { email_verify_token: '', verify: UserVerifyStatus.Verified },
                    $currentDate: {
                        updated_at: true
                    }
                }
            )
        ])
        const [accessToken, refreshToken] = token
        await databaseService.refreshToken.insertOne(
            new RefreshToken({
                user_id: new ObjectId(userId),
                token: refreshToken
            })
        )
        return {
            accessToken,
            refreshToken
        }
    }

    async resendVerifyEmail(userId: string) {
        const emailVerifyToken = await this.signEmailVerifyToken({
            userId,
            verify: UserVerifyStatus.Unverified
        })
        console.log('Resend Verify email', emailVerifyToken)

        await databaseService.users.updateOne(
            { _id: new ObjectId(userId) },
            {
                $set: {
                    email_verify_token: emailVerifyToken
                },
                $currentDate: {
                    updated_at: true
                }
            }
        )
        return {
            message: 'Resend verify email successfully'
        }
    }

    async forgotPassword({ userId, verify }: { userId: string; verify: UserVerifyStatus }) {
        const forgotPasswordToken = await this.signForgotPasswordToken({
            userId,
            verify
        })
        await databaseService.users.updateOne(
            { _id: new ObjectId(userId) },
            {
                $set: {
                    forgot_password_token: forgotPasswordToken.toString()
                },
                $currentDate: {
                    updated_at: true
                }
            }
        )

        console.log('Forgot password token: ', forgotPasswordToken)
        return {
            message: 'Check email to reset password successfully'
        }
    }

    async resetPassword(userId: string, password: string) {
        await databaseService.users.updateOne(
            { _id: new ObjectId(userId) },
            {
                $set: {
                    password: hashPassword(password),
                    forgot_password_token: ''
                },
                $currentDate: {
                    updated_at: true
                }
            }
        )
        return {
            message: 'Reset password successfully'
        }
    }

    async getMe(userId: string) {
        const user = await databaseService.users.findOne(
            { _id: new ObjectId(userId) },
            {
                projection: {
                    password: 0,
                    email_verify_token: 0,
                    forgot_password_token: 0
                }
            }
        )

        return user
    }

    async updateMe(userId: string, payload: UpdateMeRequestBody) {
        const _payload = payload.date_of_birth
            ? { ...payload, date_of_birth: new Date(payload.date_of_birth) }
            : payload
        databaseService.users.findOneAndUpdate(
            { _id: new ObjectId(userId) },
            {
                $set: {
                    _payload
                },
                $currentDate: {
                    updated_at: true
                }
            }
        )
    }

    async profile(username: string) {
        const user = await databaseService.users.findOne(
            { username: username },
            {
                projection: {
                    password: 0,
                    email_verify_token: 0,
                    forgot_password_token: 0,
                    verify: 0,
                    created_at: 0,
                    updated_at: 0
                }
            }
        )

        if (!user) {
            throw new ErrorWithStatus({
                message: 'User not found',
                status: 404
            })
        }
        return user
    }

    async follow({ userId, followed_user_id }: { userId: string; followed_user_id: string }) {
        const follower = await databaseService.followers.findOne({
            user_id: new ObjectId(userId),
            followed_user_id: followed_user_id
        })

        if (!follower) {
            await databaseService.followers.insertOne(
                new Followers({
                    user_id: new ObjectId(userId),
                    followed_user_id: followed_user_id
                })
            )
            return {
                message: 'Follow successfully'
            }
        }
        return {
            message: 'This User was followed'
        }
    }

    async unFollow({ userId, followed_user_id }: { userId: string; followed_user_id: string }) {
        const follower = await databaseService.followers.findOne({
            user_id: new ObjectId(userId),
            followed_user_id: followed_user_id
        })

        if (follower) {
            await databaseService.followers.deleteOne({
                user_id: new ObjectId(userId),
                followed_user_id: followed_user_id
            })
            return {
                message: 'Unfollow successfully'
            }
        }

        return {
            message: 'This User was not followed'
        }
    }

    async changePassword({
        userId,
        old_password,
        new_password
    }: {
        userId: string
        old_password: string
        new_password: string
    }) {
        const hash_new_password = hashPassword(new_password)
        const hash_oldpassword = hashPassword(old_password)

        const user = await databaseService.users.findOne({
            _id: new ObjectId(userId)
        })

        if (!user) {
            throw new ErrorWithStatus({
                message: 'User not found',
                status: 404
            })
        }

        const is_match: boolean = hash_oldpassword == user.password

        if (!is_match) {
            throw new ErrorWithStatus({
                message: 'Old password not match',
                status: 400
            })
        }

        await databaseService.users.updateOne(
            { _id: new ObjectId(userId) },
            {
                $set: {
                    password: hash_new_password
                },
                $currentDate: {
                    updated_at: true
                }
            }
        )

        return {
            message: 'Change password successfully'
        }
    }
}

const userService = new UserService()
export default userService
