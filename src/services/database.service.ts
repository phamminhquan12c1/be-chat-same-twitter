import { Collection, Db, MongoClient } from 'mongodb'
import dotenv from 'dotenv'
import User from '~/models/schemas/User.schema'
import RefreshToken from '~/models/schemas/RefreshToken.schema'
import Followers from '~/models/schemas/Followers.schema'
dotenv.config()

const uri =
    'mongodb+srv://' +
    process.env.DB_USERNAME +
    ':' +
    process.env.DB_PWD +
    '@tweeter.sjftxwx.mongodb.net/?retryWrites=true&w=majority'

class DatabaseService {
    private client: MongoClient
    private db: Db
    constructor() {
        this.client = new MongoClient(uri)
        this.db = this.client.db(process.env.DB_NAME)
    }
    async connect() {
        try {
            // Send a ping to confirm a successful connection
            await this.db.command({ ping: 1 })
            console.log('Pinged your deployment. You successfully connected to MongoDB!')
        } catch (error) {
            console.log('Error', error)
            throw error
        }
    }

    get users(): Collection<User> {
        return this.db.collection('users')
    }

    get refreshToken(): Collection<RefreshToken> {
        return this.db.collection('refresh_tokens')
    }

    get followers(): Collection<Followers> {
        return this.db.collection('followers')
    }
}

const databaseService = new DatabaseService()

export default databaseService
