import { Request } from 'express'
import fs from 'fs'
import path from 'path'
import sharp from 'sharp'
import { getNameFromFullName, handleUploadSingleImage } from '~/utils/files'

class MediaService {
    async handleUploadSingleImage(req: Request) {
        const file = await handleUploadSingleImage(req)
        const new_name = getNameFromFullName(file.newFilename)
        const new_path = path.resolve('src/uploads', `${new_name}.jpeg`)
        const info = await sharp(file.filepath).jpeg().toFile(new_path)
        fs.unlinkSync(file.filepath)
        return info
    }
}

const mediaService = new MediaService()
export default mediaService
