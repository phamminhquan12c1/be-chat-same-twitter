import { NextFunction, Request, Response } from 'express'
import { checkSchema } from 'express-validator'
import { JsonWebTokenError } from 'jsonwebtoken'
import { ObjectId } from 'mongodb'
import { UserVerifyStatus } from '~/constants/enums/user.enum'
import { REGEX_USERNAME } from '~/constants/regex'
import { ErrorWithStatus } from '~/models/Errors'
import { TokenPayload } from '~/models/requests/users.requests'
import databaseService from '~/services/database.service'
import userService from '~/services/user.service'
import { hashPassword } from '~/utils/crypto'
import { verifyToken } from '~/utils/jwt'
import { validate } from '~/utils/validation'

export const registerValidator = validate(
    checkSchema(
        {
            name: {
                notEmpty: {
                    errorMessage: 'Name is required'
                },
                isString: {
                    errorMessage: 'Name must be a string'
                },
                isLength: {
                    options: {
                        min: 0,
                        max: 75
                    },
                    errorMessage: 'Name must be between 0 and 75 characters'
                },
                trim: true
            },
            email: {
                notEmpty: {
                    errorMessage: 'Email is required'
                },
                isEmail: {
                    errorMessage: 'Email must be a valid email'
                },
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        const isExistEmail = await userService.checkExistEmail(value)
                        if (isExistEmail) {
                            throw new Error('Email already exists')
                        }
                        return true
                    }
                }
            },
            password: {
                notEmpty: {
                    errorMessage: 'Password is required'
                },
                isString: {
                    errorMessage: 'Password must be a string'
                },
                isLength: {
                    options: {
                        min: 6,
                        max: 50
                    },
                    errorMessage: 'Password must be between 6 and 50 characters'
                }
            },
            date_of_birth: {
                notEmpty: {
                    errorMessage: 'Date of birth is required'
                },
                isISO8601: {
                    options: {
                        strict: true,
                        strictSeparator: true
                    },
                    errorMessage: 'Date of birth must be a valid date'
                }
            }
        },
        ['body']
    )
)

export const loginValidator = validate(
    checkSchema(
        {
            email: {
                notEmpty: {
                    errorMessage: 'Email is required'
                },
                isEmail: {
                    errorMessage: 'Email must be a valid email'
                },
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        const isExistEmail = await databaseService.users.findOne({
                            email: value,
                            password: hashPassword(req.body.password)
                        })
                        if (!isExistEmail) {
                            throw new Error('Email or password is incorrect')
                        }
                        req.user = isExistEmail
                        return true
                    }
                }
            },
            password: {
                notEmpty: {
                    errorMessage: 'Password is required'
                },
                isString: {
                    errorMessage: 'Password must be a string'
                },
                isLength: {
                    options: {
                        min: 6,
                        max: 50
                    },
                    errorMessage: 'Password must be between 6 and 50 characters'
                }
            }
        },
        ['body']
    )
)

export const accessTokenValidator = validate(
    checkSchema(
        {
            Authorization: {
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        if (!value) {
                            throw new ErrorWithStatus({ message: 'Invalid access token', status: 401 })
                        }
                        const accessToken = (value || '').split(' ')[1]
                        if (accessToken === '') {
                            throw new ErrorWithStatus({ message: 'Invalid access token', status: 401 })
                        }

                        try {
                            const decode_authorization = await verifyToken({
                                token: accessToken,
                                secretOrPublicKey: process.env.JWT_SECRET_ACCESS_TOKEN as string
                            })
                                req.decode_authorization = decode_authorization
                        } catch (error) {
                            throw new ErrorWithStatus({ message: 'Invalid access token', status: 401 })
                        }
                        return true
                    }
                }
            }
        },
        ['headers']
    )
)

export const refreshTokenValidator = validate(
    checkSchema(
        {
            refresh_token: {
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        if (!value) {
                            throw new ErrorWithStatus({ message: 'Invalid refresh token', status: 401 })
                        }
                        try {
                            const [decode_refresh_token, refresh_token] = await Promise.all([
                                await verifyToken({
                                    token: value,
                                    secretOrPublicKey: process.env.JWT_SECRET_REFRESH_TOKEN as string
                                }),
                                await databaseService.refreshToken.findOne({
                                    token: value
                                })
                            ])
                            if (!refresh_token) {
                                throw new ErrorWithStatus({
                                    message: 'Invalid refresh token or not exist',
                                    status: 401
                                })
                            }

                            ;(req as Request).decode_refresh_token = decode_refresh_token
                        } catch (error) {
                            if (error instanceof JsonWebTokenError) {
                                throw new ErrorWithStatus({ message: 'Invalid refresh token', status: 401 })
                            }
                            throw error
                        }

                        return true
                    }
                }
            }
        },
        ['body']
    )
)

export const emailVerifyTokenValidator = validate(
    checkSchema(
        {
            email_verify_token: {
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        if (!value) {
                            throw new ErrorWithStatus({ message: 'Invalid email verify token', status: 401 })
                        }

                        try {
                            const decode_emai_verify_token = await verifyToken({
                                token: value,
                                secretOrPublicKey: process.env.JWT_SECRET_EMAIL_VERIFY_TOKEN as string
                            })

                            ;(req as Request).decode_email_verify_token = decode_emai_verify_token
                        } catch (error) {
                            throw new ErrorWithStatus({ message: (error as JsonWebTokenError).message, status: 401 })
                        }

                        return true
                    }
                }
            }
        },
        ['body']
    )
)

export const forgotPasswordValidator = validate(
    checkSchema(
        {
            email: {
                notEmpty: {
                    errorMessage: 'Email is required'
                },
                isEmail: {
                    errorMessage: 'Email must be a valid email'
                },
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        const user = await databaseService.users.findOne({
                            email: value
                        })
                        if (!user) {
                            throw new Error('Email not exist')
                        }
                        ;(req as Request).user = user
                        return true
                    }
                }
            }
        },
        ['body']
    )
)

export const verifyForgotPasswordTokendValidator = validate(
    checkSchema(
        {
            forgot_password_token: {
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        if (!value) {
                            throw new ErrorWithStatus({ message: 'Invalid forgot password token', status: 401 })
                        }

                        try {
                            const decode_forgot_password_token = await verifyToken({
                                token: value,
                                secretOrPublicKey: process.env.JWT_FORGOT_PASSWORD_TOKEN as string
                            })
                            const { userId } = decode_forgot_password_token
                            const user = await databaseService.users.findOne({
                                _id: new ObjectId(userId)
                            })
                            if (!user) {
                                throw new ErrorWithStatus({
                                    message: 'User not found',
                                    status: 401
                                })
                            }
                            if (user.forgot_password_token !== value) {
                                throw new ErrorWithStatus({
                                    message: 'Invalid forgot password token',
                                    status: 401
                                })
                            }
                        } catch (error) {
                            throw new ErrorWithStatus({ message: (error as JsonWebTokenError).message, status: 401 })
                        }
                    }
                }
            }
        },
        ['body']
    )
)

export const resetPasswordValidator = validate(
    checkSchema(
        {
            password: {
                notEmpty: {
                    errorMessage: 'Password is required'
                },
                isLength: {
                    errorMessage: 'Password must be at least 8 characters long',
                    options: {
                        min: 8
                    }
                },
                trim: true
            },
            confirm_password: {
                notEmpty: {
                    errorMessage: 'Confirm password is required'
                },
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        if (value !== req.body.password) {
                            throw new ErrorWithStatus({ message: 'Confirm password not match', status: 422 })
                        }
                        return true
                    }
                }
            },
            forgot_password_token: {
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        if (!value) {
                            throw new ErrorWithStatus({ message: 'Invalid forgot password token', status: 401 })
                        }

                        try {
                            const decode_forgot_password_token = await verifyToken({
                                token: value,
                                secretOrPublicKey: process.env.JWT_FORGOT_PASSWORD_TOKEN as string
                            })
                            const { userId } = decode_forgot_password_token
                            const user = await databaseService.users.findOne({
                                _id: new ObjectId(userId)
                            })
                            if (!user) {
                                throw new ErrorWithStatus({
                                    message: 'User not found',
                                    status: 401
                                })
                            }
                            if (user.forgot_password_token !== value) {
                                throw new ErrorWithStatus({
                                    message: 'Invalid forgot password token',
                                    status: 401
                                })
                            }
                            ;(req as Request).decode_forgot_password_token = decode_forgot_password_token
                        } catch (error) {
                            throw new ErrorWithStatus({ message: (error as JsonWebTokenError).message, status: 401 })
                        }
                    }
                }
            }
        },
        ['body']
    )
)

export const verifyUserValidator = (req: Request, res: Response, next: NextFunction) => {
    const { verify } = req.decode_authorization as TokenPayload
    if (verify !== UserVerifyStatus.Verified) {
        next(
            new ErrorWithStatus({
                message: 'User not verified',
                status: 403
            })
        )
    }
    next()
}

export const updateMeValidator = validate(
    checkSchema(
        {
            name: {
                optional: true,
                isString: {
                    errorMessage: 'Name must be a string'
                },
                trim: true
            },
            date_of_birth: {
                optional: true,
                isISO8601: {
                    options: {
                        strict: true,
                        strictSeparator: true
                    },
                    errorMessage: 'Date of birth must be a valid date'
                }
            },
            avatar: {
                optional: true,
                isString: {
                    errorMessage: 'Avatar must be a string'
                },
                trim: true
            },
            bio: {
                optional: true,
                isString: {
                    errorMessage: 'bio must be a string'
                },
                trim: true
            },
            location: {
                optional: true,
                isString: {
                    errorMessage: 'location must be a string'
                },
                trim: true
            },
            website: {
                optional: true,
                isString: {
                    errorMessage: 'location must be a string'
                },
                trim: true
            },
            username: {
                optional: true,
                isString: {
                    errorMessage: 'username must be a string'
                },
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        if (REGEX_USERNAME.test(value) === false) {
                            throw new ErrorWithStatus({
                                message: 'Invalid username',
                                status: 400
                            })
                        }

                        const user = await databaseService.users.findOne({
                            username: value
                        })

                        req.user = user

                        if (user) {
                            throw new ErrorWithStatus({
                                message: 'This username is already taken',
                                status: 409
                            })
                        }
                    }
                }
            },
            cover_photo: {
                optional: true,
                isString: {
                    errorMessage: 'username must be a string'
                },
                trim: true
            }
        },
        ['body']
    )
)

export const followValidator = validate(
    checkSchema(
        {
            followed_user_id: {
                isString: {
                    errorMessage: 'Name must be a string'
                },
                trim: true,
                notEmpty: {
                    errorMessage: 'User id is required'
                },
                custom: {
                    options: async (value: string, { req }) => {
                        if (!ObjectId.isValid(value)) {
                            throw new ErrorWithStatus({
                                message: 'Invalid followed_user_id',
                                status: 404
                            })
                        }

                        const followed_user = await databaseService.users.findOne({
                            _id: new ObjectId(value)
                        })

                        if (!followed_user) {
                            throw new ErrorWithStatus({
                                message: 'User not found',
                                status: 404
                            })
                        }
                    }
                }
            }
        },
        ['body']
    )
)

export const unfollowValidator = validate(
    checkSchema(
        {
            user_id: {
                trim: true,
                custom: {
                    options: async (value: string, { req }) => {
                        if (!ObjectId.isValid(value)) {
                            throw new ErrorWithStatus({
                                message: 'Invalid user_id',
                                status: 404
                            })
                        }

                        const followed_user = await databaseService.users.findOne({
                            _id: new ObjectId(value)
                        })

                        if (!followed_user) {
                            throw new ErrorWithStatus({
                                message: 'User not found',
                                status: 404
                            })
                        }
                    }
                }
            }
        },
        ['params']
    )
)

export const changePasswordValidator = validate(
    checkSchema(
        {
            old_password: {
                isString: {
                    errorMessage: 'Old password must be a string'
                },
                trim: true,
                notEmpty: {
                    errorMessage: 'Old password is required'
                }
            },
            new_password: {
                isString: {
                    errorMessage: 'New password must be a string'
                },
                trim: true,
                notEmpty: {
                    errorMessage: 'New password is required'
                }
            }
        },
        ['body']
    )
)
