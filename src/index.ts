import express from 'express'
import routes from './routes'
import databaseService from './services/database.service'
import dotenv from 'dotenv'
import { defaultErrorHandeler } from './middlewares/error.middewares'
import cors from 'cors'
import { initFolder } from './utils/files'
import argv from 'minimist'

const options = argv(process.argv.slice(2))

databaseService.connect()
const app: express.Application = express()
dotenv.config()
console.log(options)
initFolder()
app.use(cors())
app.use(express.json())

app.use('/api', routes)

app.use(defaultErrorHandeler)

app.listen(process.env.PORT, () => {
    console.log('Server is running on port ' + process.env.PORT)
})
