import { Router } from 'express'
import usersRoute from './users.routes'
import mediasRoute from './formidable.routes'

const router = Router()

router.use('/users', usersRoute)
router.use('/medias', mediasRoute)

export default router
