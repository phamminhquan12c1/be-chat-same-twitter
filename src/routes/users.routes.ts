import { UpdateMeRequestBody } from './../models/requests/users.requests'
import { wrapRequestHandler } from './../utils/handle'
import { Router } from 'express'
import {
    loginController,
    logoutController,
    registerController,
    emailVerifyController,
    resendEmailVerifyController,
    forgotPasswordController,
    verifyForgotPassswordTokenController,
    resetPasswordController,
    getMeController,
    updateMeController,
    getProfileController,
    followController,
    unFollowController,
    changePasswordController,
    loginGoogleController
} from '~/controllers/user.controller'
import { filterMiddleware } from '~/middlewares/common.middlewares'
import {
    loginValidator,
    registerValidator,
    accessTokenValidator,
    refreshTokenValidator,
    emailVerifyTokenValidator,
    forgotPasswordValidator,
    verifyForgotPasswordTokendValidator,
    resetPasswordValidator,
    verifyUserValidator,
    updateMeValidator,
    followValidator,
    unfollowValidator,
    changePasswordValidator
} from '~/middlewares/users.middlewares'

const usersRoute = Router()

/**
 * Description: login in application
 * path: /login
 * method: post
 * body: email, password
 */
usersRoute.post('/login', loginValidator, wrapRequestHandler(loginController))

/**
 * Description: register a new user
 * path: /register
 * method: post
 * body: name, email, password
 */
usersRoute.post('/register', registerValidator, wrapRequestHandler(registerController))

/**
 * Description: logout
 * path: /logout
 * method: post
 * header: Authorization: Bearer access_token
 * body: refresh_token
 */
usersRoute.post('/logout', accessTokenValidator, refreshTokenValidator, wrapRequestHandler(logoutController))

/**
 * Description: Verify email when user click the link in email
 * path: /email-verify
 * method: post
 * body: {email_verify_token: string}
 */
usersRoute.post('/email-verify', emailVerifyTokenValidator, wrapRequestHandler(emailVerifyController))

/**
 * Description: Resend Verify email when user click the link in email
 * path: /resend-email-verify
 * method: post
 * header: Authorization: Bearer access_token
 * body: {}
 */
usersRoute.post('/resend-email-verify', accessTokenValidator, wrapRequestHandler(resendEmailVerifyController))

/**
 * Description: Forgot password
 * path: /forgot-password
 * method: post
 * body: {email: string}
 */
usersRoute.post('/forgot-password', forgotPasswordValidator, wrapRequestHandler(forgotPasswordController))

/**
 * Description: Verify forgot password
 * path: /verify-forgot-password
 * method: post
 * body: {forgot_password_token: string}
 */
usersRoute.post(
    '/verify-forgot-password',
    verifyForgotPasswordTokendValidator,
    wrapRequestHandler(verifyForgotPassswordTokenController)
)

/**
 * Description: reset password
 * path: /reset-password
 * method: post
 * body: {forgot_password_token: string}
 */
usersRoute.post('/reset-password', resetPasswordValidator, wrapRequestHandler(resetPasswordController))

/**
 * Description: Get my profile
 * path: /me
 * method: get
 * header: Authorization: Bearer access_token
 */
usersRoute.get('/me', accessTokenValidator, wrapRequestHandler(getMeController))

/**
 * Description: Update my profile
 * path: /me
 * method: get
 * header: Authorization: Bearer access_token
 * body: UserSchema
 */
usersRoute.patch(
    '/me',
    accessTokenValidator,
    verifyUserValidator,
    updateMeValidator,
    filterMiddleware<UpdateMeRequestBody>([
        'name',
        'date_of_birth',
        'avatar',
        'bio',
        'location',
        'website',
        'username',
        'cover_photo'
    ]),
    wrapRequestHandler(updateMeController)
)

/**
 * Description: Get user info
 * path: /:username
 * method: get
 * header: Authorization: Bearer access_token
 */
usersRoute.get('/:username', wrapRequestHandler(getProfileController))

/**
 * Description: Follow someone
 * path: /follow
 * method: post
 * header: Authorization: Bearer access_token
 * body: {user_id: string}
 */
usersRoute.post(
    '/follow',
    accessTokenValidator,
    verifyUserValidator,
    followValidator,
    wrapRequestHandler(followController)
)

/**
 * Description: Un Follow someone
 * path: /follow/:user_id
 * method: post
 * header: Authorization: Bearer access_token
 * body: {user_id: string}
 */
usersRoute.delete(
    '/follow/:user_id',
    accessTokenValidator,
    verifyUserValidator,
    unfollowValidator,
    wrapRequestHandler(unFollowController)
)

/**
 * Description: Change my password
 * path: /change-password
 * method: post
 * header: Authorization: Bearer access_token
 * body: {old_password: string, new_password: string}
 */
usersRoute.post(
    '/change-password',
    accessTokenValidator,
    verifyUserValidator,
    changePasswordValidator,
    wrapRequestHandler(changePasswordController)
)

/**
 * Description: Login with google
 * path: /oauth/google
 * method: post
 * header: Authorization: Bearer access_token
 * body: {old_password: string, new_password: string}
 */
usersRoute.get('/oauth/google', wrapRequestHandler(loginGoogleController))

export default usersRoute
