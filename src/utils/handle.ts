import { NextFunction, Request, RequestHandler, Response } from 'express'

export const wrapRequestHandler = <p>(fn: RequestHandler<p>) => {
    return async (req: Request<p>, res: Response, next: NextFunction) => {
        try {
            await fn(req, res, next)
        } catch (error) {
            next(error)
        }
    }
}
