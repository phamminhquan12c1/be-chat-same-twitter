import { Request } from 'express'
import { File } from 'formidable'
import fs from 'fs'
import path from 'path'

export const initFolder = () => {
    if (!fs.existsSync(path.resolve('./src/uploads/temp'))) {
        fs.mkdirSync(path.resolve('./src/uploads/temp'), {
            recursive: true
        })
    }
}

export const handleUploadSingleImage = async (req: Request) => {
    const formidable = (await import('formidable')).default
    const form = formidable({
        uploadDir: path.resolve('src/uploads/temp'),
        maxFiles: 1,
        keepExtensions: true,
        maxFileSize: 5 * 1024 * 1024,
        filter: function ({ name, originalFilename, mimetype }) {
            const valid = name === 'image' && Boolean(mimetype?.includes('image'))
            if (!valid) {
                form.emit('error' as any, new Error('Invalid file type') as any)
            }
            return valid
        }
    })
    return new Promise<File>((resolve, reject) => {
        form.parse(req, async (err, fields, files) => {
            if (err) {
                return reject(err)
            }

            // eslint-disable-next-line no-extra-boolean-cast
            if (!Boolean(files.image)) {
                return reject(new Error('File not found'))
            }
            resolve((files.image as File[])[0])
        })
    })
}

export const getNameFromFullName = (fullName: string) => {
    const nameArray = fullName.split('.')
    nameArray.pop()
    return nameArray.join('')
}
