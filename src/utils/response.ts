export const dataResponse = (statusCode: number, data: any, message: string) => {
    let status: string = 'success'

    if (statusCode >= 400 && statusCode < 500) {
        status = 'fail'
    }

    return {
        statusCode: statusCode,
        status: status,
        message: message,
        data: data
    }
}
