import HTTP_STATUS from '~/constants/httpStatus'
import USER_MESSAGES from '~/constants/userMessages'

type ErrrosType = Record<
    string,
    {
        msg: string
        [key: string]: any
    }
>

export class ErrorWithStatus {
    message: string
    status: number
    constructor({ message, status }: { message: string; status: number }) {
        this.message = message
        this.status = status
    }
}

export class EntityExistError extends ErrorWithStatus {
    errors: any
    constructor({ errors }: { errors: ErrrosType }) {
        super({ message: USER_MESSAGES.VALIDATION_ERROR, status: HTTP_STATUS.UNPROCESSABLE_ENTITY })
        this.errors = errors
    }
}
