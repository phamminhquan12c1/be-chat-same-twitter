import { ObjectId } from 'mongodb'

interface FollowersType {
    _id?: ObjectId
    user_id: ObjectId
    followed_user_id: string
    created_ad?: Date
}

export default class Followers {
    _id?: ObjectId
    user_id: ObjectId
    followed_user_id: string
    created_ad?: Date
    constructor({ _id, user_id, followed_user_id, created_ad }: FollowersType) {
        this._id = _id
        this.user_id = user_id
        this.followed_user_id = followed_user_id
        this.created_ad = created_ad || new Date()
    }
}
