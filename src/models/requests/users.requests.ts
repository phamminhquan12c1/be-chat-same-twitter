import { JwtPayload } from 'jsonwebtoken'
import { TokenType } from '~/constants/enums/user.enum'
import { ParamsDictionary } from 'express-serve-static-core'
export interface UpdateMeRequestBody {
    name?: string
    date_of_birth?: string
    avatar?: string
    bio?: string
    location?: string
    website?: string
    username?: string
    cover_photo?: string
}

export interface UserRegisterRequest {
    name: string
    email: string
    password: string
    date_of_birth: string
}

export interface TokenPayload extends JwtPayload {
    user_id: string
    token_type: TokenType
}

export interface LogoutRequestBody {
    refresh_token: string
}

export interface ForgotRequestPayload {
    email: string
}

export interface VerifyForgotPasswordTokenRequestPayload {
    forgot_password_token: string
}

export interface resetPasswordPayload {
    forgot_password_token: string
    password: string
    confirm_password: string
}

export interface ParamsGetProfileRequestPayload {
    username: string
}

export interface ParamsUnfollowRequestPayload extends ParamsDictionary {
    user_id: string
}

export interface FollowRequestBody {
    followed_user_id: string
}

export interface ChangPasswordRequestBody {
    old_password: string
    new_password: string
}
