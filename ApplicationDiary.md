22/02/2024
- Initial source base 
- Setup files Route
- Create User Schema

23/02/2024
- Create UserController
- Add a function Register(hash password,...) 
- Add validate Request function

24/02/2024
- JWT
- Validate advanced with validate schema
- Optimize source base

25/02/2024
- Add advanced errors handler
- Build logout API
- Build API forgot password

26/02/2024
- Build Email Validate API
- Build API resend email forgot password
- Build api verify forgot password
- build api reset password
- Validate MongoDB

27/02/2024
- Build Filter Middleware to get fields, which need get
- Build API get my information
- Build API update my information
- Build API get info 
- Build API Follow
- BUild API Unfollow
- Build API Change password




